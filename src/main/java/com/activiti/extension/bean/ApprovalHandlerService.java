package com.activiti.extension.bean;

import com.cassavasmartech.agentmanagementapi.service.approval.ApprovalHandler;
import com.cassavasmartech.agentmanagementapi.service.approval.ApprovalType;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.JavaDelegate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.Map;

import static com.cassavasmartech.agentmanagementapi.service.approval.ApprovalConstants.VariableNames.APPROVAL_TYPE;
import static com.cassavasmartech.agentmanagementapi.service.approval.ApprovalConstants.VariableNames.RECORD_IDENTIFIER;
import static com.cassavasmartech.agentmanagementapi.service.approval.ApprovalConstants.VariableNames.TASK_APPROVED;
import static com.cassavasmartech.agentmanagementapi.service.approval.ApprovalConstants.VariableNames.TASK_COMPLETED_BY;

@Component("approvalHandlerService")
@Slf4j
@RequiredArgsConstructor
public class ApprovalHandlerService  {

    private final List<ApprovalHandler> approvalHandlers;

    @PostConstruct
    public void init(){
        log.info("ApprovalHandlerService: {}", this.approvalHandlers);
    }

    public void execute(DelegateExecution execution) {

        Map<String, Object> variables = execution.getVariables();

        final String approvalTypeValue = execution.getVariable(APPROVAL_TYPE, String.class);

        log.info("processing approval: {}", approvalTypeValue);

        final long entityId = execution.getVariable(RECORD_IDENTIFIER, Long.class);

        final ApprovalType approvalType = ApprovalType.valueOf(approvalTypeValue);

        final boolean accepted = execution.getVariable(TASK_APPROVED, Boolean.class);

        approvalHandlers.stream()
                .filter(h -> h.canApprove(approvalType))
                .findFirst()
                .orElseThrow(() -> new RuntimeException("Approval Handler not configured."))
                .handleApproval(accepted, approvalType, variables);
    }
}
