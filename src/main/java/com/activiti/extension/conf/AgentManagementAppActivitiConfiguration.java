package com.activiti.extension.conf;

import com.activiti.extension.bean.ApprovalHandlerService;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan( basePackageClasses = {ApprovalHandlerService.class})
public class AgentManagementAppActivitiConfiguration {
}
