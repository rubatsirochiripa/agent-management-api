package com.cassavasmartech.agentmanagementapi.repository;

import com.cassavasmartech.agentmanagementapi.model.TransactionType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface TransactionTypeRepository extends JpaRepository<TransactionType, Long> {
    Optional<TransactionType> findByCode(String transactionTypeCode);
}
