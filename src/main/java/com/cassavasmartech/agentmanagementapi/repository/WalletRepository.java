package com.cassavasmartech.agentmanagementapi.repository;

import com.cassavasmartech.agentmanagementapi.model.Wallet;
import com.cassavasmartech.agentmanagementapi.model.AccountStatus;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface WalletRepository extends JpaRepository<Wallet,Long> {

    Optional<Wallet> findByAccountNumberAndAccountStatus(final String accountNumber, final AccountStatus status);

    List<Wallet> findByAgent_ContactInfo_MobileNumber(final String mobileNumber);
}
