package com.cassavasmartech.agentmanagementapi.repository;

import com.cassavasmartech.agentmanagementapi.model.Commission;
import com.cassavasmartech.agentmanagementapi.model.Currency;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface CommissionRepository extends JpaRepository<Commission,Long> {
}
