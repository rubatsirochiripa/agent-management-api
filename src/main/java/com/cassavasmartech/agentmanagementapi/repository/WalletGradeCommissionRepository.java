package com.cassavasmartech.agentmanagementapi.repository;

import com.cassavasmartech.agentmanagementapi.model.WalletGradeCommission;
import com.cassavasmartech.agentmanagementapi.model.WalletGradeKey;
import org.springframework.data.jpa.repository.JpaRepository;

public interface WalletGradeCommissionRepository extends JpaRepository<WalletGradeCommission, WalletGradeKey> {
}
