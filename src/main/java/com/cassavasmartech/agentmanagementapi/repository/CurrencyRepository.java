package com.cassavasmartech.agentmanagementapi.repository;

import com.cassavasmartech.agentmanagementapi.model.Currency;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface CurrencyRepository extends JpaRepository<Currency,Long> {
    Optional<Currency> findByAlphaCode(final String code);
    Optional<Currency> findByNumericCode(final int numericCode);

}
