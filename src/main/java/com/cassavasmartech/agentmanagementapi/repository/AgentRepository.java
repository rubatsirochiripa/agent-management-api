package com.cassavasmartech.agentmanagementapi.repository;

import com.cassavasmartech.agentmanagementapi.model.AccountStatus;
import com.cassavasmartech.agentmanagementapi.model.Agent;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface AgentRepository extends JpaRepository<Agent,Long> {

    @Query("select c from Agent c where lower(c.idNumber) = lower(:registrationNumber) and c.company = :company")
    List<Agent> findByRegistrationNumberAndCompany(@Param("registrationNumber")String registrationNumber , @Param("company")boolean company);

    @Query("select c from Agent c where lower(c.name) = lower(:name) and c.company = :company")
    List<Agent> findByNameAndCompany(@Param("name")String name , @Param("company")boolean company);

    Optional<Agent> findByContactInfo_MobileNumber(final String mobileNumber);

    Optional<Agent> findByIdNumber(final String idNumber);

    Page<Agent> findByNameContainingIgnoreCase(String term, Pageable pageable);

    Page<Agent> findByStatus(AccountStatus accountStatus, Pageable pageable);

   Page<Agent> findByIdNumberContainingOrContactInfo_MobileNumberContaining(final String idNumber, String mobileNumber, Pageable pageable);

}
