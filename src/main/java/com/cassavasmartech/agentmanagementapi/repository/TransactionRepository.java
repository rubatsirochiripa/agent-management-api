package com.cassavasmartech.agentmanagementapi.repository;

import com.cassavasmartech.agentmanagementapi.model.Transaction;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Date;
import java.util.Optional;

public interface TransactionRepository extends JpaRepository<Transaction, Long> {

    Optional<Transaction> findBySystemReference(final String systemReference);

    Optional<Transaction> findBySourceReferenceAndDeviceId(final String systemReference, final String deviceId);

    Page<Transaction> findByWallet_IdAndTransactionDateBetween(long walletId,Date fromDate, Date toDate, Pageable page);
}
