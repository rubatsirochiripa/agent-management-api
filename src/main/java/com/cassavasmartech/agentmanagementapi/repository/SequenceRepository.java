package com.cassavasmartech.agentmanagementapi.repository;

@FunctionalInterface
public interface SequenceRepository {
    long getNextSequenceValue(String sequenceName, long initialValue);
}
