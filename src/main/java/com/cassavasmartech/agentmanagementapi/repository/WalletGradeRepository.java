package com.cassavasmartech.agentmanagementapi.repository;

import com.cassavasmartech.agentmanagementapi.model.WalletGrade;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface WalletGradeRepository extends JpaRepository<WalletGrade, Long> {
   Optional<WalletGrade> findByDefaultGradeTrue();
}
