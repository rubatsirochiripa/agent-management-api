package com.cassavasmartech.agentmanagementapi;

import com.activiti.extension.conf.AgentManagementAppActivitiConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.web.config.EnableSpringDataWebSupport;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication(exclude = {org.activiti.spring.boot.SecurityAutoConfiguration.class},
scanBasePackageClasses = {AgentManagementAppActivitiConfiguration.class, AgentManagementApiApplication.class})
@EnableTransactionManagement
@EnableJpaRepositories
@EnableAsync
@EnableSpringDataWebSupport
public class AgentManagementApiApplication {
    public static void main(String[] args) {
        SpringApplication.run(AgentManagementApiApplication.class, args);
    }
}
