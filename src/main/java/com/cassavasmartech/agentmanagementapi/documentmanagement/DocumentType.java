package com.cassavasmartech.agentmanagementapi.documentmanagement;

import lombok.Data;

import java.io.Serializable;

@Data
public class DocumentType implements Serializable {
    private int id;
    private String label;
}
