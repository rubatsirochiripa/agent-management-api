package com.cassavasmartech.agentmanagementapi.documentmanagement;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.ArrayList;
import java.util.List;

public class RestPageImpl<T> extends PageImpl<T> {

    @JsonCreator(mode = JsonCreator.Mode.PROPERTIES)
    public RestPageImpl(@JsonProperty("results") List<T> results,
                        @JsonProperty("number") int page,
                        @JsonProperty("size") int size,
                        @JsonProperty("totalElements") long total) {
        super(results, new PageRequest(page, size), total);
    }

    public RestPageImpl(List<T> results, Pageable pageable, long total) {
        super(results, pageable, total);
    }

    public RestPageImpl(List<T> results) {
        super(results);
    }

    public RestPageImpl() {
        super(new ArrayList());
    }
}
