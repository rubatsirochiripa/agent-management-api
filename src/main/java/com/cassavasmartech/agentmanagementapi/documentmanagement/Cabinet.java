package com.cassavasmartech.agentmanagementapi.documentmanagement;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
@JsonInclude(value = JsonInclude.Include.NON_NULL)
public class Cabinet {
    private Long id;
    private Long parent;
    private String label;
    public Cabinet() {
    }
    public Cabinet(String label) {
        this.label = label;
    }
}
