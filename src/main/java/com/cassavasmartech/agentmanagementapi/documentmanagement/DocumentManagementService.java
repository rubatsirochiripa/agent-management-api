package com.cassavasmartech.agentmanagementapi.documentmanagement;

import org.springframework.web.multipart.MultipartFile;

import java.util.Optional;

public interface DocumentManagementService {

    Optional<Cabinet> createCabinet(String label);

    void uploadDocument(Long identifier, MultipartFile file) throws Exception;
}
