package com.cassavasmartech.agentmanagementapi.documentmanagement;

import com.cassavasmartech.agentmanagementapi.api.data.CabinetDocuments;
import com.cassavasmartech.agentmanagementapi.api.data.CabinetDocumentsResult;
import com.cassavasmartech.agentmanagementapi.api.data.DocumentUploadRequest;
import com.cassavasmartech.agentmanagementapi.api.data.DocumentUploadResponse;
import com.cassavasmartech.agentmanagementapi.api.data.UploadedDocument;
import com.cassavasmartech.agentmanagementapi.util.HeadersBuilder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;
import static java.util.stream.Collectors.toList;

@Service
@Slf4j
public class DocumentManagementServiceImpl {

    @Value("${agent-management-file-tmp-location}")
    private  String transactionUploadRoot;
    @Autowired
    @Qualifier("documentsRestTemplate")
    private RestTemplate documentsRestTemplate;

    @Value("${mayan-create-cabinet}")
    private String cabinetsUrl;

    @Value("${mayan-auth-token}")
    private String authorisationToken;

    @Value("${mayan-document-types}")
    private String documentTypesUrl;

    @Value("${mayan-cabinet-document}")
    private String cabinetDocumentsUrl;

    @Value("${mayan-upload-document}")
    private String documentsUrl;

    @Value("${mayan-download-document}")
    private String downloadUrl;

    @Value("${mayan-document-upload-delay-mills}")
    private long uploadDelayInMills;

    @Value("${agent-system-document-download-url}")
    private String downloadUrlTemplate;

    public DocumentTypeResponse getDocumentTypes() {
        final HttpEntity<?> requestEntity = new HttpEntity<>(getHeaders());
        ResponseEntity<DocumentTypeResponse> result = documentsRestTemplate.exchange(documentTypesUrl, HttpMethod.GET, requestEntity, DocumentTypeResponse.class);
        return result.getBody();
    }

    public List<UploadedDocument> findCabinetDocuments(long cabinetId) {
        final HttpEntity<?> requestEntity = new HttpEntity<>(getHeaders());
        final ResponseEntity<CabinetDocuments> responseEntity = documentsRestTemplate.exchange(
                cabinetDocumentsUrl, HttpMethod.GET, requestEntity, CabinetDocuments.class, cabinetId);
        return responseEntity.getBody()
                .getResults()
                .stream()
                .map(this::map)
                .collect(toList());
    }

    private UploadedDocument map(final CabinetDocumentsResult result) {
        final UploadedDocument uploadedDocument = new UploadedDocument();
        uploadedDocument.setDocumentTypeLabel(result.getDocumentType().getLabel());
        uploadedDocument.setDownloadUrl(resolveDownloadLink(result.getId()));
        uploadedDocument.setId(result.getId());
        uploadedDocument.setLabel(result.getLabel());
        return uploadedDocument;
    }

    private String resolveDownloadLink(final long documentId) {
        return UriComponentsBuilder.fromUriString(downloadUrlTemplate)
                .buildAndExpand(documentId).toUriString();
    }

    public Optional<Cabinet> createCabinet(final String label) {
        try {
            final Cabinet cabinet = new Cabinet(label);
            final HttpEntity<?> requestEntity = new HttpEntity<>(cabinet, getHeaders());
            final ResponseEntity<Cabinet> preferenceResponseResponseEntity = documentsRestTemplate.exchange(
                    cabinetsUrl, HttpMethod.POST, requestEntity, Cabinet.class);
            return Optional.ofNullable(preferenceResponseResponseEntity.getBody());
        } catch (Exception ex) {
            log.warn("Cabinet Creation Failed", ex);
            return Optional.empty();
        }
    }

    private String saveToTemp(Long identifier, MultipartFile file) throws IOException {
        final String fileName = StringUtils.cleanPath(file.getOriginalFilename());
        Path path = Paths.get(fileFor(identifier), fileName);
        createDirectoryIfNotExists(path);
        try {
            Files.copy(file.getInputStream(), path, REPLACE_EXISTING);
        } catch (IOException e) {
            log.error("", e);
        }
        return path.toString();
    }

    private void createDirectoryIfNotExists(Path path) throws IOException {
        try {
            Files.createDirectories(path);
        }
        catch (FileAlreadyExistsException ex){
            log.warn("{}", ex.getMessage());
        }
    }

    private String fileFor(Long id) {
        return this.transactionUploadRoot + "_" + id;
    }

    public int uploadDocument(Long documentTypeId, Long cabinetId, MultipartFile file) throws Exception {
        final String fileName = saveToTemp(documentTypeId, file);
        return uploadDocument(fileName, cabinetId, documentTypeId);
    }

    private int uploadDocument(final String fileName, final Long cabinetId, final Long documentTypeId) {
        log.info("Uploading file: {}", fileName);
        MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
        body.add("file", new FileSystemResource(fileName));
        body.add("document_type", documentTypeId);
        final HttpEntity<MultiValueMap<String, Object>> request = new HttpEntity<>(body, getUploadHttpHeaders());
        final ResponseEntity<DocumentUploadResponse> response = documentsRestTemplate.postForEntity(documentsUrl, request, DocumentUploadResponse.class, cabinetId);
        return response.getBody().getId();

    }

    @Async
    public void addFileToCabinet(Long cabinetId, int documentId) {
        log.info("Adding file to cabinet: cabinetId:{}, documentId:{}", cabinetId, documentId);
        try {
            Thread.sleep(uploadDelayInMills);
        } catch (InterruptedException e) {
            log.warn("",e);
        }
        log.info("Adding file to cabinet: cabinetId:{}, documentId:{}", cabinetId, documentId);
        final DocumentUploadRequest uploadRequest = new DocumentUploadRequest();
        uploadRequest.setDocumentslist(String.valueOf(documentId));

        final HttpEntity<?> requestEntity = new HttpEntity<>(uploadRequest, getHeaders());
        ResponseEntity<String> addToCabinetResponse = documentsRestTemplate.postForEntity(
                cabinetDocumentsUrl, requestEntity, String.class, cabinetId);
        log.info("Document added to cabinet.{}", addToCabinetResponse.getBody());
    }

    private HttpHeaders getUploadHttpHeaders() {
        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers.set(HttpHeaders.AUTHORIZATION, authorisationToken);
        return headers;
    }

    private HttpHeaders getHeaders() {
        HttpHeaders requestHeaders = HeadersBuilder.createRequestHeaders();
        requestHeaders.set(HttpHeaders.AUTHORIZATION, authorisationToken);
        return requestHeaders;
    }

    public ResponseEntity downloadFile(long documentId) throws IOException {
        HttpEntity<String> entity = new HttpEntity<>(getDownloadHttpHeaders());
        final ResponseEntity<byte[]> response = documentsRestTemplate.exchange(downloadUrl,
                HttpMethod.GET, entity, byte[].class, documentId, documentId);
        return response;
    }

    private HttpHeaders getDownloadHttpHeaders() {
        final HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_OCTET_STREAM, MediaType.ALL));
        headers.set(HttpHeaders.AUTHORIZATION, authorisationToken);
        return headers;
    }

}
