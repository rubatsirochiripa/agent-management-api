package com.cassavasmartech.agentmanagementapi.model;

import lombok.Data;
import org.hibernate.envers.Audited;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

@Entity
@Data
@Audited
public class Agent extends BaseEntity {

    @Column(name = "name", nullable = false, length = 100)
    private String name;

    @Column(name = "surname", nullable = false, length = 100)
    private String surname;

    private Boolean company;

    @Column(name = "id_number", nullable = false, length = 100)
    private String idNumber;

    @Embedded
    private ContactInfo contactInfo;

    private String approvalProcessInstanceId;

    @Enumerated(EnumType.STRING)
    private AccountStatus status = AccountStatus.NEW;

    private Long fileCabinetId;

    @ManyToOne
    private Currency defaultCurrency;

    @Transient
    public String getFullName() {
        final StringBuilder nameBuilder = new StringBuilder();
        if (name != null) {
            nameBuilder.append(name);
        }
        if (surname != null) {
            if (nameBuilder.length() > 0) {
                nameBuilder.append(" ");
            }
            nameBuilder.append(surname);
        }
        return nameBuilder.toString();
    }

    public static Agent copyOf(Agent agent) {
        final Agent copy = new Agent();
        copy.setCompany(agent.getCompany());
        copy.setContactInfo(agent.getContactInfo());
        copy.setDateCreated(agent.getDateCreated());
        copy.setDateModified(agent.getDateModified());
        copy.setId(agent.getId());
        copy.setName(agent.getName());
        copy.setIdNumber(agent.getIdNumber());
        copy.setSurname(agent.getSurname());
        copy.setVersion(agent.getVersion());
        return copy;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Agent) {
            Agent other = (Agent) obj;
            if (other.getCompany() != this.getCompany()) {
                return false;
            }
            if (other.getCompany().booleanValue()) {
                return other.getName() != null && other.getName().equalsIgnoreCase(this.getName());
            }
            return other.getIdNumber() != null
                    && other.getIdNumber().equalsIgnoreCase(this.getIdNumber());

        }
        return false;
    }

    @Override
    public int hashCode() {
        if (this.getCompany().booleanValue()) {
            return this.getName().hashCode();
        }
        return this.getIdNumber().hashCode();
    }
}
