package com.cassavasmartech.agentmanagementapi.model;

public enum CommissionStatus {
    PENDING,POSTED,REVERSED;
}
