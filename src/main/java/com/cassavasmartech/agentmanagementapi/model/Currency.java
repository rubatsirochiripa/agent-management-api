package com.cassavasmartech.agentmanagementapi.model;

import lombok.Data;
import org.hibernate.envers.Audited;

import javax.persistence.Entity;

@Data
@Entity
@Audited
public class Currency  extends BaseEntity{
    private int numericCode;
    private String alphaCode;
}
