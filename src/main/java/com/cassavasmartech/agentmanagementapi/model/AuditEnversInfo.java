package com.cassavasmartech.agentmanagementapi.model;

import com.cassavasmartech.agentmanagementapi.service.audit.UserRevisionListener;
import lombok.Data;
import org.hibernate.envers.DefaultRevisionEntity;
import org.hibernate.envers.RevisionEntity;

import javax.persistence.Entity;

@Entity
@Data
@RevisionEntity(UserRevisionListener.class)
public class AuditEnversInfo extends DefaultRevisionEntity {
    private String userId;
    private String fullName;
}
