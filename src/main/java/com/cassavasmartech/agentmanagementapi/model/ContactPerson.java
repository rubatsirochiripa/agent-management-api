package com.cassavasmartech.agentmanagementapi.model;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
@Data
public class ContactPerson implements Serializable {
    @Column(name = "contact_name", length = 100)
    private String name;

    @Column(name = "contact_mobile", length = 50)
    private String mobile;
}
