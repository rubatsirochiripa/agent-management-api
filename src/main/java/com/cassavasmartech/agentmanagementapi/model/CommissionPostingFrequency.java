package com.cassavasmartech.agentmanagementapi.model;

public enum  CommissionPostingFrequency {
    INSTANT, SCHEDULED;
}
