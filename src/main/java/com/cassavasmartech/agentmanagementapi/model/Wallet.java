package com.cassavasmartech.agentmanagementapi.model;

import lombok.Data;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Data
@Audited
public class Wallet extends BaseEntity {

    @Column(unique=true)
    private String accountNumber;

    @Enumerated(EnumType.STRING)
    private AccountType accountType;

    private BigDecimal balance = BigDecimal.ZERO;

    @Enumerated(EnumType.STRING)
    private AccountStatus accountStatus = AccountStatus.NEW;

    @ManyToOne
    @NotAudited
    private Currency currency;

    @ManyToOne
    private Agent agent;

    @ManyToOne
    private WalletGrade grade;

    public Wallet(){
    }

    public Wallet(String accountNumber, Agent agent,Date auditDate, Currency currency, WalletGrade grade, AccountStatus status) {
        super(auditDate, auditDate);
        this.accountNumber = accountNumber;
        this.currency = currency;
        this.agent = agent;
        this.grade  = grade;
        this.accountStatus = status;
    }

    public BigDecimal getBalance() {
        return balance==null? BigDecimal.ZERO:balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }
}
