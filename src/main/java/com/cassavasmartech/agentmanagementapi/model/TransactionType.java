package com.cassavasmartech.agentmanagementapi.model;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Data
@Entity
public class TransactionType  extends BaseEntity {
    private String name;
    private String code;
    @Enumerated(EnumType.STRING)
    private TransactionEffect transactionEffect;
    private boolean negativeBalanceAllowed;
}
