package com.cassavasmartech.agentmanagementapi.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;


import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import java.time.LocalDateTime;
import java.util.Date;

@MappedSuperclass
@Data
public abstract class BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreated;
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateModified;
    @Version
    @JsonIgnore
    private Long version;

    public BaseEntity() {
    }

    public BaseEntity(Date dateCreated , Date dateModified){
        this.dateCreated = dateCreated;
        this.dateModified = dateModified;
    }

    public BaseEntity(long id , Date dateCreated , Date dateModified){
        this.id = id;
        this.dateCreated = dateCreated;
        this.dateModified = dateModified;
    }

}
