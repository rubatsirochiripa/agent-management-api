package com.cassavasmartech.agentmanagementapi.model;

import java.math.BigDecimal;
import java.math.RoundingMode;

public enum  CommissionCalculationMode {
    FIXED{
        public BigDecimal calculate(BigDecimal rate, BigDecimal amount){
            return rate;
        }
    } ,PERCENTAGE{
        public BigDecimal calculate(BigDecimal rate, BigDecimal amount){
            return amount.multiply(rate).divide(new BigDecimal("100"), RoundingMode.HALF_UP);
        }
    };
    public abstract BigDecimal calculate(BigDecimal rate, BigDecimal amount);
}
