package com.cassavasmartech.agentmanagementapi.model;

public enum AccountStatus {
    NEW,ACTIVE, CLOSED, SUSPENDED, REJECTED;
}
