package com.cassavasmartech.agentmanagementapi.model;

import lombok.Data;

import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
@Data
public class WalletGradeKey implements Serializable {
    @ManyToOne
    private WalletGrade walletGrade;

    @ManyToOne
    private TransactionType transactionType;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        WalletGradeKey that = (WalletGradeKey) o;
        return walletGrade.getId() == that.walletGrade.getId()
                && transactionType.getId() == that.transactionType.getId();

    }

    @Override
    public int hashCode() {
        return Objects.hash(walletGrade, transactionType);
    }
}
