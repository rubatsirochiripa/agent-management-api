package com.cassavasmartech.agentmanagementapi.model;

import lombok.Data;

@Data
public class ClientKey {
    private String keyValue;
    private boolean company;

    public ClientKey(String keyValue, boolean company) {
        this.keyValue = keyValue;
        this.company = company;
    }
}
