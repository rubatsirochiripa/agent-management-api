package com.cassavasmartech.agentmanagementapi.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Transient;
import javax.persistence.Version;
import java.math.BigDecimal;

@Entity
@Data
public class WalletGradeCommission {

    @Enumerated(EnumType.STRING)
    CommissionCalculationMode calculationMode;

    @EmbeddedId
    private WalletGradeKey id;

    private BigDecimal rate;

    @Version
    @JsonIgnore
    private Long version;

    @Transient
    public BigDecimal calculate(BigDecimal amount) {
        return this.calculationMode.calculate(this.rate, amount);
    }

}
