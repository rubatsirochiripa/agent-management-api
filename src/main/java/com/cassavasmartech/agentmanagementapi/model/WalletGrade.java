package com.cassavasmartech.agentmanagementapi.model;

import lombok.Data;
import org.hibernate.envers.Audited;

import javax.persistence.Entity;

@Entity
@Data
@Audited
public class WalletGrade extends BaseEntity {
    private String name;
    private boolean defaultGrade;
}
