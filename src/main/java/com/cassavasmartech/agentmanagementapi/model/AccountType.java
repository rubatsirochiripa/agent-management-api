package com.cassavasmartech.agentmanagementapi.model;

public enum AccountType {

    COMPANY("X", "Company"), INDIVIDUAL("I", "INDIVIDUAL");

    private final String prefix;
    private final String value;

    AccountType(String prefix, String value) {
        this.prefix = prefix;
        this.value = value;
    }

    public String getPrefix() {
        return prefix;
    }

    public String getValue() {
        return value;
    }
}
