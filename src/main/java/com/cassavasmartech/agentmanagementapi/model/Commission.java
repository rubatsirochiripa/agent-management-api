package com.cassavasmartech.agentmanagementapi.model;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import java.math.BigDecimal;

@Data
@Entity
public class Commission extends BaseEntity {

    private BigDecimal amount;

    private BigDecimal rate;

    @ManyToOne
    private Transaction transaction;

    @Enumerated(EnumType.STRING)
    private CommissionStatus commissionStatus;

    @OneToOne
    private Transaction commissionTransaction;
}
