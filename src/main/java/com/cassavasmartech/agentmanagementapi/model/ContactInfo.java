package com.cassavasmartech.agentmanagementapi.model;

import lombok.Data;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Embedded;
import java.io.Serializable;

@Embeddable
@Data
public class ContactInfo  implements Serializable {
    private String email;
    private String telephone;
    private String mobileNumber;
    @Embedded
    private ContactPerson contactPerson;
    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "addressLine1", column = @Column(name = "postal_address_line1", length = 100)),
            @AttributeOverride(name = "addressLine2", column = @Column(name = "postal_address_line2", length = 100)),
            @AttributeOverride(name = "addressLine3", column = @Column(name = "postal_address_line3", length = 100)),
            @AttributeOverride(name = "addressLine4", column = @Column(name = "postal_address_line4", length = 100))})
    private Address postalAddress;

    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "addressLine1", column = @Column(name = "physical_address_line1", length = 100)),
            @AttributeOverride(name = "addressLine2", column = @Column(name = "physical_address_line2", length = 100)),
            @AttributeOverride(name = "addressLine3", column = @Column(name = "physical_address_line3", length = 100)),
            @AttributeOverride(name = "addressLine4", column = @Column(name = "physical_address_line4", length = 100))})
    private Address physicalAddress;
}
