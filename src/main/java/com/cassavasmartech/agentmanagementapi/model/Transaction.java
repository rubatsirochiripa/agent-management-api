package com.cassavasmartech.agentmanagementapi.model;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "_transaction")
@Data
@Builder
@AllArgsConstructor
public class Transaction  extends BaseEntity {

    @ManyToOne(optional = false)
    @JoinColumn(name = "tran_type_id", nullable = false)
    private TransactionType transactionType;

    @ManyToOne(optional = false)
    @JoinColumn(name = "account_id", nullable = false)
    private Wallet wallet;

    @Temporal(TemporalType.TIMESTAMP)
    private Date transactionDate;

    @Column(name = "amount", nullable = false)
    private BigDecimal amount;

    @Column(length = 100)
    private String narration;

    @Column(length = 100)
    private String sourceReference;

    @Column(name = "reversal", nullable = false)
    private boolean reversal;

    @ManyToOne
    @JoinColumn(name = "reversed_transaction_id")
    @JsonIgnore
    private Transaction reversedTransaction;

    @Column(name = "reversed", nullable = false)
    private boolean reversed;

    @Column(length = 100)
    private String systemReference;

    private String deviceId;

    public Transaction(TransactionType transactionType, Wallet wallet, BigDecimal amount,
                       Date transactionDate, String narration, Date dateCreated, String sourceReference, String deviceId) {
        super(dateCreated, dateCreated);
        this.transactionType = transactionType;
        this.wallet = wallet;
        this.amount = amount;
        this.transactionDate = transactionDate==null? dateCreated: transactionDate;
        this.narration = narration;
        this.reversal = false;
        this.sourceReference = sourceReference;
        this.deviceId = deviceId;
    }

    public Transaction(TransactionType transactionType, Wallet wallet, BigDecimal amount, BigDecimal reserveAmount,
                       Date transactionDate, String narration, Date dateCreated, boolean reversal,
                       Transaction reversedTransaction) {
        super(dateCreated, dateCreated);
        this.transactionType = transactionType;
        this.wallet = wallet;
        this.amount = amount;
        this.transactionDate = transactionDate;
        this.narration = narration;
        this.reversal = reversal;
        this.reversedTransaction = reversedTransaction;
    }

}
