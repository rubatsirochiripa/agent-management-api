package com.cassavasmartech.agentmanagementapi.model;

import java.math.BigDecimal;

public enum  TransactionEffect {
    DEBIT(-1), CREDIT(1);

    private final int factor;

    private TransactionEffect(int factor){
        this.factor = factor;
    }

    public BigDecimal effectiveAmount(BigDecimal suppliedAmount){
        return suppliedAmount.multiply(BigDecimal.valueOf(factor));
    }
}
