package com.cassavasmartech.agentmanagementapi.model;

import lombok.Data;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
@Data
public class Address implements Serializable {

    private String addressLine1;
    private String addressLine2;
    private String addressLine3;
    private String addressLine4;

    public String generateFullAddress(String delimiter) {
        final StringBuilder fullAddressBuilder = new StringBuilder();
        if (addressLine1 != null) {
            fullAddressBuilder.append(addressLine1);
        }
        if (addressLine2 != null) {
            if (fullAddressBuilder.length() > 0) {
                fullAddressBuilder.append(delimiter);
            }
            fullAddressBuilder.append(addressLine2);
        }
        if (addressLine3 != null) {
            if (fullAddressBuilder.length() > 0) {
                fullAddressBuilder.append(delimiter);
            }
            fullAddressBuilder.append(addressLine3);
        }
        if (addressLine4 != null) {
            if (fullAddressBuilder.length() > 0) {
                fullAddressBuilder.append(delimiter);
            }
            fullAddressBuilder.append(addressLine4);
        }
        return fullAddressBuilder.toString();
    }
}
