package com.cassavasmartech.agentmanagementapi.api;

import com.cassavasmartech.agentmanagementapi.exception.InsufficientBalanceException;
import lombok.extern.slf4j.Slf4j;
import org.keycloak.common.VerificationException;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.ws.rs.NotFoundException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.FORBIDDEN;

@Order(Ordered.HIGHEST_PRECEDENCE)
//@ControllerAdvice
@RestControllerAdvice
@Slf4j
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

    @ResponseStatus(BAD_REQUEST)
    @ExceptionHandler({Exception.class})
    @ResponseBody
    protected ResponseEntity<Object> handleBusinessException(
            Exception ex) {
        return new ResponseEntity<>(new ApiError(ex.getLocalizedMessage()), BAD_REQUEST);
    }

    @ResponseStatus(FORBIDDEN)
    @ExceptionHandler({VerificationException.class, AuthenticationException.class})
    @ResponseBody
    protected ResponseEntity<Object> handleBadCredentials(
            VerificationException ex) {
        return new ResponseEntity<>(new ApiError(ex.getMessage()), FORBIDDEN);
    }

    @ResponseStatus(BAD_REQUEST)
    @ExceptionHandler(InsufficientBalanceException.class)
    @ResponseBody
    protected ResponseEntity<Object> handleInsufficientBalance(
            VerificationException ex) {
        return new ResponseEntity<>(new ApiError(ex.getMessage()), BAD_REQUEST);
    }

    @ExceptionHandler(value = NotFoundException.class)
    public ResponseEntity<CustomErrorResponse> handleGenericNotFoundException(NotFoundException e) {
        CustomErrorResponse error = new CustomErrorResponse("NOT_FOUND_ERROR", e.getMessage());
        error.setTimestamp(LocalDateTime.now());
        error.setStatus((HttpStatus.NOT_FOUND.value()));
        return new ResponseEntity<>(error, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(Exception.class)
    public final ResponseEntity<CustomErrorResponse> handleAllExceptions(Exception ex, WebRequest request) {
        CustomErrorResponse error = new CustomErrorResponse("Internal Server Error", ex.getMessage());
        error.setTimestamp(LocalDateTime.now());
        error.setStatus((HttpStatus.INTERNAL_SERVER_ERROR.value()));
        error.setErrorMsg(ex.getMessage());
        return new ResponseEntity<>(error, HttpStatus.INTERNAL_SERVER_ERROR);
    }

}
