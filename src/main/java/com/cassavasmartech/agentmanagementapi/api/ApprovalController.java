package com.cassavasmartech.agentmanagementapi.api;

import com.cassavasmartech.agentmanagementapi.service.approval.ApprovalService;
import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("process/{processInstanceId}/approvals")
@Slf4j
@Api(value = "approvals")
@CrossOrigin(maxAge = 3600, origins = "*")
@RequiredArgsConstructor
public class ApprovalController {

    private final ApprovalService approvalService;

    @PostMapping("/{approvalId}")
    public ResponseEntity approve(@PathVariable("approvalId") final String taskId,
                                  @PathVariable("processInstanceId") final String processInstanceId,
                                  @RequestBody ApprovalRequest approvalRequest){
        log.info("processing approval: {}", approvalRequest);
        approvalService.completeTask(taskId,processInstanceId,approvalRequest);
        return ResponseEntity.ok().build();
    }
}
