package com.cassavasmartech.agentmanagementapi.api;

import com.cassavasmartech.agentmanagementapi.documentmanagement.DocumentManagementServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

@CrossOrigin(maxAge = 3600, origins = "*")
@RestController()
@RequiredArgsConstructor
public class DocumentsController {

    private final DocumentManagementServiceImpl documentManagementService;

    @GetMapping("/documents/{documentId}/download")
    public ResponseEntity fetchFile(@PathVariable long documentId) throws IOException {
        return documentManagementService.downloadFile(documentId);
    }

}
