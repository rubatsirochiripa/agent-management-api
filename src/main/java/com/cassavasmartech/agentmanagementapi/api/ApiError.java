package com.cassavasmartech.agentmanagementapi.api;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import org.springframework.http.HttpStatus;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.ZoneId;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ApiError implements Serializable {
    private Long timestamp;
    private String message;

    public ApiError(String message) {
        timestamp = LocalDateTime.now().atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();
        this.message= message;
    }
}