package com.cassavasmartech.agentmanagementapi.api;

import com.cassavasmartech.agentmanagementapi.api.data.AgentApproval;
import com.cassavasmartech.agentmanagementapi.api.data.WalletRequest;
import com.cassavasmartech.agentmanagementapi.model.Wallet;
import com.cassavasmartech.agentmanagementapi.model.AccountStatus;
import com.cassavasmartech.agentmanagementapi.model.WalletGrade;
import com.cassavasmartech.agentmanagementapi.repository.WalletGradeRepository;
import com.cassavasmartech.agentmanagementapi.service.wallet.WalletService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.net.URI;
import java.util.List;
import java.util.Optional;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/agents")
@Api(value = "wallet")
@CrossOrigin(maxAge = 3600, origins = "*")
public class WalletController {

    private final WalletService walletService;

    private final WalletGradeRepository walletGradeRepository;

    @PostMapping("/{agentId}/wallets")
    public ResponseEntity create( @RequestBody final WalletRequest request, @PathVariable Long agentId) {
        final Wallet wallet = walletService.create(request, agentId);
        return ResponseEntity.created(URI.create("/accounts/" + wallet.getId())).body(wallet);
    }

//    @GetMapping("/wallets/{walletIdentifer}")
//    public ResponseEntity find(@PathVariable final String walletIdentifer){
//        return getFindByAccountNumber(walletIdentifer);
//    }

    @PutMapping("/wallets/{walletId}")
    public ResponseEntity update(@PathVariable final long walletId, @RequestBody final WalletRequest request){
        log.info("Updating agent wallet: {},{}", walletId, request);
        return ResponseEntity.ok().build();
    }

    private ResponseEntity getFindByAccountNumber(final String accountNumber) {
        Optional<Wallet> optional = walletService.findByAccountNumberAndStatus(accountNumber, AccountStatus.ACTIVE);
        if (optional.isPresent()) {
            return ResponseEntity.ok(optional.get());
        }
        return ResponseEntity.notFound().build();
    }

    @GetMapping("/walletgrades")
    public Page<WalletGrade> findApprovals(@ApiParam Pageable pageable){
        return walletGradeRepository.findAll(pageable);
    }

    @GetMapping("/wallets/{mobileNumber}")
    public List<Wallet> find(@PathVariable final String mobileNumber){
        return walletService.findByMobileNumber(mobileNumber);
    }

}
