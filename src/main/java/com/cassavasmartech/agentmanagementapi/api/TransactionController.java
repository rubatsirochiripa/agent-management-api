package com.cassavasmartech.agentmanagementapi.api;

import com.cassavasmartech.agentmanagementapi.api.data.FindTransactionRequest;
import com.cassavasmartech.agentmanagementapi.api.data.TransactionRequest;
import com.cassavasmartech.agentmanagementapi.documentmanagement.DocumentManagementServiceImpl;
import com.cassavasmartech.agentmanagementapi.documentmanagement.DocumentType;
import com.cassavasmartech.agentmanagementapi.exception.BusinessException;
import com.cassavasmartech.agentmanagementapi.model.Transaction;
import com.cassavasmartech.agentmanagementapi.service.transaction.TransactionService;
import com.cassavasmartech.agentmanagementapi.util.Constants;
import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static com.cassavasmartech.agentmanagementapi.util.Constants.AppHeaders.X_APP_ID;
import static com.cassavasmartech.agentmanagementapi.util.Constants.AppHeaders.X_HEADER_DEVICE_ID;


@Api(value = "wallet transactions")
@RestController
@RequestMapping("/transactions")
@RequiredArgsConstructor
@CrossOrigin(maxAge = 3600, origins = "*")
@Slf4j
public class TransactionController {

    private final TransactionService transactionService;
    private final DocumentManagementServiceImpl documentManagementService;
    private String transactionUploadRoot;

    @PostMapping
    @ResponseStatus(value = HttpStatus.OK)
    public ResponseEntity post(@RequestBody TransactionRequest request, @RequestHeader HttpHeaders headers) {
        final String appId = String.valueOf(headers.get(X_APP_ID));
        final String deviceId = getHeaderValue( headers, X_HEADER_DEVICE_ID);
        return ResponseEntity.ok(transactionService.process(request, deviceId));
    }

    private String getHeaderValue(HttpHeaders headers,final String key) {
        List<String> values = headers.get(key);
        if(!CollectionUtils.isEmpty(values)){
            return values.get(0);
        }
        return "";
    }

    private String getRequiredHeaderValue(HttpHeaders headers,final String key) {
        List<String> values = headers.get(key);
        if(CollectionUtils.isEmpty(values)){
           throw new BusinessException("Missing required header " + key);
        }
        return values.get(0);
    }

    @PostMapping("/reverse")
    @ResponseStatus(value = HttpStatus.OK)
    public ResponseEntity autoreverse(@RequestBody TransactionRequest request, @RequestHeader(X_HEADER_DEVICE_ID) String deviceId) {
        Transaction reverse = transactionService.reverse(request, deviceId);
        return ResponseEntity.ok(reverse);
    }

    @PutMapping("/{transactionId}")
    @ResponseStatus(value = HttpStatus.OK)
    public ResponseEntity reverse(@RequestBody TransactionRequest request, @PathVariable Long transactionId) {
        Transaction reverse = transactionService.reverse(transactionId, request.getNarration());
        return ResponseEntity.ok(reverse);
    }

    @PostMapping("/wallet/{walletId}")
    @ResponseStatus(value = HttpStatus.OK)
    public Page<Transaction> findAgentTransactions(@PathVariable Long walletId, @RequestBody FindTransactionRequest request, Pageable pageable) {
        log.info("find wallet transactions: {}", pageable);
        return  transactionService.findWalletTransactions(walletId, request, pageable);
    }

    @GetMapping("/{transactionId}")
    @ResponseStatus(value = HttpStatus.OK)
    public ResponseEntity findById(@PathVariable Long transactionId) {
        final Transaction transaction = transactionService.findOne(transactionId);
        if (transaction == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(transaction);
    }

    @PostMapping("/{transactionId}/documents/{documentType}")
    @ResponseStatus(value = HttpStatus.OK)
    public ResponseEntity upload(@PathVariable Long transactionId, @PathVariable DocumentType documentType) {
        final Transaction transaction = transactionService.findOne(transactionId);
        if (transaction == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(transaction);
    }

    @GetMapping("/{transactionId}/documents")
    @ResponseStatus(value = HttpStatus.OK)
    public ResponseEntity downloadLinks(@PathVariable Long transactionId) {
        final Transaction transaction = transactionService.findOne(transactionId);
        if (transaction == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(transaction);
    }


}
