package com.cassavasmartech.agentmanagementapi.api;

import com.cassavasmartech.agentmanagementapi.api.data.UserCredentials;
import com.cassavasmartech.agentmanagementapi.service.keycloak.KeyCloakService;
import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.keycloak.representations.AccessToken;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static com.cassavasmartech.agentmanagementapi.service.keycloak.KeyCloakSecurityContextHolder.getRequiredUserToken;
import static org.springframework.http.HttpStatus.UNAUTHORIZED;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/auth")
@Api(value = "auth")
@Slf4j
@CrossOrigin(maxAge = 3600, origins = "*")
public class AuthController {

    private final KeyCloakService keyClockService;

     @PostMapping(value = "/token")
    public ResponseEntity<?> getTokenUsingCredentials(@RequestBody UserCredentials userCredentials) throws Exception {
         try {
             return keyClockService.getToken(userCredentials);
         }
         catch (Exception ex){
             return new ResponseEntity<>(new ApiError(ex.getLocalizedMessage()), UNAUTHORIZED);
         }
    }

    @PostMapping(value = "/logout")
    public ResponseEntity<?> logout()  {
        log.info("Logging out - 1");
        final AccessToken token = getRequiredUserToken();
        log.info("Logging out - 2: {}", token.getSubject());
        keyClockService.logoutUser(token.getSubject());
        return  ResponseEntity.ok("Logout successfully");
    }

    @PostMapping(value = "/refresh")
    public ResponseEntity refresh(@RequestBody UserCredentials userCredentials) {
         log.info("Getting refresh token: {}", userCredentials);
        return keyClockService.getByRefreshToken(userCredentials.getRefreshToken());
    }

}
