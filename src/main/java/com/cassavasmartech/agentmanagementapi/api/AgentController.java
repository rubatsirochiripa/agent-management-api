package com.cassavasmartech.agentmanagementapi.api;

import com.cassavasmartech.agentmanagementapi.api.data.AgentApproval;
import com.cassavasmartech.agentmanagementapi.api.data.CreateAgentRequest;
import com.cassavasmartech.agentmanagementapi.model.Agent;
import com.cassavasmartech.agentmanagementapi.service.agent.AgentDocumentation;
import com.cassavasmartech.agentmanagementapi.service.agent.AgentServiceImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequiredArgsConstructor
@RequestMapping("/agents")
@Slf4j
@Api(value = "agents")
@CrossOrigin(maxAge = 3600, origins = "*")
public class AgentController {

    private final AgentServiceImpl clientService;

    private final AgentDocumentation agentDocumentation;

    @PostMapping
    public ResponseEntity create( @RequestBody final CreateAgentRequest client) {
        Agent savedAgent = clientService.create(client);
        return ResponseEntity.ok(savedAgent);
    }

    @GetMapping
    public Page<Agent> search(@RequestParam(required = false, defaultValue = "%") String idNumber,@RequestParam(required = false, defaultValue = "%") String mobileNumber,
                              @ApiParam Pageable pageable){
        return clientService.findByIdNumberOrMobile(idNumber,mobileNumber,pageable);
    }

    @GetMapping("/approvals")
    public Page<AgentApproval> findApprovals(@RequestParam(required = false, defaultValue = "") String mobileNumber,@ApiParam Pageable pageable){
        return clientService.findApprovals(mobileNumber, pageable);
    }

    @GetMapping("/{id}")
    public Agent findById(final @PathVariable long id){
        return clientService.findById(id);
    }

    @PutMapping("/{id}")
    public Agent update(@RequestBody final CreateAgentRequest client){
        log.info("updating client: {}", client);
        return null;
    }

    @PostMapping("/{id}/documents/{documentTypeId}")
    public ResponseEntity upload(@PathVariable  Long id, @PathVariable Long documentTypeId, @RequestParam("file") MultipartFile file) throws Exception {
        log.info("Uploading document: {}, {}", id, documentTypeId);
        agentDocumentation.upload(id,documentTypeId,file);
        return ResponseEntity.ok().build();
    }

    @PostMapping("/{id}/proof-of-funds-document/{documentTypeId}")
    public ResponseEntity uploadProofOfFunds(@PathVariable  Long id, @PathVariable Long documentTypeId, @RequestParam("file") MultipartFile file) throws Exception {
        log.info("Uploading proof of funds document");
        agentDocumentation.upload(id,documentTypeId,file);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/{id}/documents")
    public ResponseEntity agentDocuments(@PathVariable long id){
        log.info("finding documents:{}", id);
        try {
            return ResponseEntity.ok(agentDocumentation.findAgentDocuments(id));
        }
        catch (Exception ex){
            log.error("",ex);
            return ResponseEntity.badRequest().body(ex.getMessage());
        }
    }

}
