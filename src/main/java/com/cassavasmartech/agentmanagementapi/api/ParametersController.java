package com.cassavasmartech.agentmanagementapi.api;

import com.cassavasmartech.agentmanagementapi.documentmanagement.DocumentManagementServiceImpl;
import com.cassavasmartech.agentmanagementapi.documentmanagement.DocumentTypeResponse;
import com.cassavasmartech.agentmanagementapi.model.Currency;
import com.cassavasmartech.agentmanagementapi.model.TransactionType;
import com.cassavasmartech.agentmanagementapi.repository.CurrencyRepository;
import com.cassavasmartech.agentmanagementapi.repository.TransactionTypeRepository;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping()
@RequiredArgsConstructor
@CrossOrigin(maxAge = 3600, origins = "*")
@Slf4j
public class ParametersController {

    private final CurrencyRepository currencyRepository;
    private final DocumentManagementServiceImpl documentManagementService;
    private final TransactionTypeRepository transactionTypeRepository;

    @GetMapping("/currencies")
    public Page<Currency> findAll(@ApiParam Pageable pageable){
        return currencyRepository.findAll(pageable);
    }

    @GetMapping("/document-types")
    public DocumentTypeResponse getDocumentTypes(){
        log.info("Getting documentation");
        return documentManagementService.getDocumentTypes();
    }


    @GetMapping("/transaction-types")
    public List<TransactionType> getAllTransactionTypes(){
        log.info("Getting documentation");
        return transactionTypeRepository.findAll();
    }

}
