package com.cassavasmartech.agentmanagementapi.api;

import com.cassavasmartech.agentmanagementapi.api.data.ApprovalStatus;
import lombok.Data;

@Data
public class ApprovalRequest {
    private String comment;
    private ApprovalStatus status;
}
