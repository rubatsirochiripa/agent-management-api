package com.cassavasmartech.agentmanagementapi.api.data;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Data
public class DocumentLatestVersion implements Serializable {
    @JsonProperty("download_url")
    private String downloadUrl;
    @JsonProperty("encoding")
    private String encoding;
    @JsonProperty("mimetype")
    private String mimetype;
    @JsonProperty("pages_url")
    private String pagesUrl;
    @JsonProperty("size")
    private Integer size;
}
