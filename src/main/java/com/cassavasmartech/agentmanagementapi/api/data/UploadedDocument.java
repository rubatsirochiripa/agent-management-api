package com.cassavasmartech.agentmanagementapi.api.data;

import lombok.Data;

import java.io.Serializable;

@Data
public class UploadedDocument  implements Serializable {
    private int id;
    private String label;
    private String downloadUrl;
    private String documentTypeLabel;
}
