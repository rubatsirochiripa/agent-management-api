package com.cassavasmartech.agentmanagementapi.api.data;

import com.cassavasmartech.agentmanagementapi.model.AccountType;
import lombok.Data;

import java.io.Serializable;

@Data
public class AccountHolder implements Serializable {
    private final AccountType accountType;
    private final String mobileNumber;
    private final String holderName;

    public AccountHolder(AccountType accountType, String mobileNumber, String holderName) {
        this.accountType = accountType;
        this.mobileNumber = mobileNumber;
        this.holderName = holderName;
    }

}
