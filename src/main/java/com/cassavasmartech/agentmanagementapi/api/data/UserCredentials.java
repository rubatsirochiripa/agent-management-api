package com.cassavasmartech.agentmanagementapi.api.data;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.io.Serializable;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserCredentials  implements Serializable {
    private String password;
    private String username;
    private String refreshToken;
}
