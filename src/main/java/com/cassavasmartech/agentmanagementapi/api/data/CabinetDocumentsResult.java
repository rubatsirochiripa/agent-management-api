package com.cassavasmartech.agentmanagementapi.api.data;

import com.cassavasmartech.agentmanagementapi.documentmanagement.DocumentType;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Data
public class CabinetDocumentsResult implements Serializable {
    @JsonProperty("document_type")
    private DocumentType documentType;
    @JsonProperty("id")
    private Integer id;
    @JsonProperty("label")
    private String label;
    @JsonProperty("latest_version")
    private DocumentLatestVersion latestVersion;

}
