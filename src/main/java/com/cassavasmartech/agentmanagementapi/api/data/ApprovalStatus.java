package com.cassavasmartech.agentmanagementapi.api.data;

public enum ApprovalStatus {

    APPROVE (true), REJECT(false);

    private final boolean approved;

    ApprovalStatus(boolean approved) {
        this.approved = approved;
    }

    public boolean isApproved() {
        return approved;
    }
}
