package com.cassavasmartech.agentmanagementapi.api.data;

import lombok.Data;

import java.io.Serializable;

@Data
public class UserDTO implements Serializable {
    private String userName;
    private String emailAddress;
    private String firstName;
    private String lastName;
    private String password;
}
