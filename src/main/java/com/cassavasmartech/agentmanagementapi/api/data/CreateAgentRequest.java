package com.cassavasmartech.agentmanagementapi.api.data;

import com.cassavasmartech.agentmanagementapi.model.ContactInfo;
import lombok.Data;

import java.io.Serializable;

@Data
public class CreateAgentRequest implements Serializable {
    private String name;
    private String surname;
    private ContactInfo contactInfo;
    private String idNumber;
    private Boolean company;
    private int currencyCode;
}
