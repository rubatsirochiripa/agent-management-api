package com.cassavasmartech.agentmanagementapi.api.data;

import com.cassavasmartech.agentmanagementapi.model.Agent;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class AgentApproval implements Serializable {
    private Agent agent;
    private List<TaskRepresentation> tasks;
}
