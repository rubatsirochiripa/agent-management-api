package com.cassavasmartech.agentmanagementapi.api.data;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class FindTransactionRequest  implements Serializable {
    private Date fromDate;
    private Date toDate;
}
