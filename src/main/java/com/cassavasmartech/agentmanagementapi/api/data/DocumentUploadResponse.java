package com.cassavasmartech.agentmanagementapi.api.data;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
public  class DocumentUploadResponse implements Serializable {

    @JsonProperty("description")
    private String description;

    @JsonProperty("document_type")
    private int documentType;

    @JsonProperty("id")
    private int id;

    @JsonProperty("label")
    private String label;

    @JsonProperty("language")
    private String language;


}