package com.cassavasmartech.agentmanagementapi.api.data;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class TransactionRequest implements Serializable {
    private final String mobileNumber;
    private final String transactionTypeCode;
    private final BigDecimal amount;
    private final String narration;
    private final String sourceReference;
    private final int currencyCode;

    public TransactionRequest(String mobileNumber, String transactionTypeCode,
                              BigDecimal amount,
                              String narration, String sourceReference,
                              int currencyCode
                                     ) {
        this.mobileNumber = mobileNumber;
        this.transactionTypeCode = transactionTypeCode;
        this.amount = amount;
        this.narration = narration;
        this.sourceReference = sourceReference;
        this.currencyCode = currencyCode;
    }
}
