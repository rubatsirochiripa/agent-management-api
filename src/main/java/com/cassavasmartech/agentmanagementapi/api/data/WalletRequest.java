package com.cassavasmartech.agentmanagementapi.api.data;

import com.cassavasmartech.agentmanagementapi.model.AccountStatus;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class WalletRequest implements Serializable {
    private final String currencyCode;
    private final AccountStatus accountStatus;
    private long walletGradeId;
    public WalletRequest(AccountStatus accountStatus, String currencyCode, long walletGradeId) {
        this.accountStatus = accountStatus;
        this.currencyCode = currencyCode;
        this.walletGradeId =walletGradeId;
    }
}
