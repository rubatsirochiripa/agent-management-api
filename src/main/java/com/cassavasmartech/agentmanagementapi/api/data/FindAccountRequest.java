package com.cassavasmartech.agentmanagementapi.api.data;

import lombok.Data;

@Data
public class FindAccountRequest {
    private String mobileNumber;
    private int currencyCode;
}
