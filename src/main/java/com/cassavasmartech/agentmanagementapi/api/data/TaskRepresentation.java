package com.cassavasmartech.agentmanagementapi.api.data;

import lombok.Data;

import java.io.Serializable;

@Data
public class TaskRepresentation implements Serializable {
    private String taskId;
    private String name;
    private String assignee;
    private String description;
    private String processInstanceId;


    public TaskRepresentation() {
    }

    public TaskRepresentation(String taskId, String name, String assignee, String description, String processInstanceId) {
        this.taskId = taskId;
        this.name = name;
        this.assignee = assignee;
        this.description = description;
        this.processInstanceId = processInstanceId;
    }
}
