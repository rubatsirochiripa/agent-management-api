package com.cassavasmartech.agentmanagementapi.service.audit;

import com.cassavasmartech.agentmanagementapi.model.AuditEnversInfo;
import com.cassavasmartech.agentmanagementapi.service.keycloak.KeyCloakSecurityContextHolder;
import org.hibernate.envers.RevisionListener;

public class UserRevisionListener implements RevisionListener {
    @Override
    public void newRevision(Object revisionEntity) {
        final AuditEnversInfo auditEnversInfo = (AuditEnversInfo) revisionEntity;
        KeyCloakSecurityContextHolder.getAccessToken().ifPresent(t -> {
            auditEnversInfo.setUserId(t.getPreferredUsername());
            auditEnversInfo.setFullName(t.getName());
        });
    }


}
