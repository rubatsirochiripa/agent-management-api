package com.cassavasmartech.agentmanagementapi.service.transaction;

import com.cassavasmartech.agentmanagementapi.api.data.FindTransactionRequest;
import com.cassavasmartech.agentmanagementapi.api.data.TransactionRequest;
import com.cassavasmartech.agentmanagementapi.model.Transaction;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;

public interface TransactionService {
    @Transactional
    Transaction process(TransactionRequest transactionRequest, String deviceId);

    Transaction reverse(Long transactionId, String narration);

    Transaction reverse(final TransactionRequest request, String devideId);

    Transaction findOne(Long transactionId);

    Page<Transaction> findWalletTransactions(final Long walletId, final FindTransactionRequest request, Pageable pageable);
}
