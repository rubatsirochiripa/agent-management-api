package com.cassavasmartech.agentmanagementapi.service.transaction;

import com.cassavasmartech.agentmanagementapi.api.data.FindTransactionRequest;
import com.cassavasmartech.agentmanagementapi.api.data.TransactionRequest;
import com.cassavasmartech.agentmanagementapi.exception.BusinessException;
import com.cassavasmartech.agentmanagementapi.exception.InsufficientBalanceException;
import com.cassavasmartech.agentmanagementapi.model.AccountStatus;
import com.cassavasmartech.agentmanagementapi.model.Commission;
import com.cassavasmartech.agentmanagementapi.model.CommissionStatus;
import com.cassavasmartech.agentmanagementapi.model.Currency;
import com.cassavasmartech.agentmanagementapi.model.Transaction;
import com.cassavasmartech.agentmanagementapi.model.TransactionType;
import com.cassavasmartech.agentmanagementapi.model.Wallet;
import com.cassavasmartech.agentmanagementapi.model.WalletGradeCommission;
import com.cassavasmartech.agentmanagementapi.model.WalletGradeKey;
import com.cassavasmartech.agentmanagementapi.repository.CommissionRepository;
import com.cassavasmartech.agentmanagementapi.repository.CurrencyRepository;
import com.cassavasmartech.agentmanagementapi.repository.TransactionRepository;
import com.cassavasmartech.agentmanagementapi.repository.TransactionTypeRepository;
import com.cassavasmartech.agentmanagementapi.repository.WalletGradeCommissionRepository;
import com.cassavasmartech.agentmanagementapi.repository.WalletRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.Optional;

@Service
@Slf4j
@RequiredArgsConstructor
public class TransactionServiceImpl implements TransactionService {
    private final WalletRepository walletRepository;
    private final TransactionRepository transactionRepository;
    private final TransactionTypeRepository transactionTypeRepository;
    private final CurrencyRepository currencyRepository;
    private final WalletGradeCommissionRepository walletGradeCommissionRepository;
    private final CommissionRepository commissionRepository;

    @Override
    @Transactional
    public Transaction process(TransactionRequest transactionRequest, String deviceId) {
        final Date auditDate = new Date();
        final Optional<Currency> optionalCurrency = currencyRepository.findByNumericCode(transactionRequest.getCurrencyCode());
        if (!optionalCurrency.isPresent()) {
            throw new BusinessException("invalid currency");
        }
        final String accountNumber = transactionRequest.getMobileNumber().trim() + transactionRequest.getCurrencyCode();

        Optional<Wallet> optionalAccount = walletRepository.findByAccountNumberAndAccountStatus(accountNumber, AccountStatus.ACTIVE);
        if (!optionalAccount.isPresent()) {
            throw new BusinessException("invalid account -" + accountNumber);
        }
        final Wallet wallet = optionalAccount.get();
        final TransactionType transactionType = transactionTypeRepository
                .findByCode(transactionRequest.getTransactionTypeCode())
                .orElseThrow(() -> new BusinessException("invalid transaction type"));

        if (transactionRequest.getAmount().compareTo(BigDecimal.ZERO) < 0) {
            throw new BusinessException("invalid transaction amount");
        }

        final BigDecimal amountToEffect = transactionType.getTransactionEffect()
                .effectiveAmount(transactionRequest.getAmount());

        BigDecimal walletBalance = wallet.getBalance().add(amountToEffect);

        if(!transactionType.isNegativeBalanceAllowed() && walletBalance.compareTo(BigDecimal.ZERO) < 0){
            throw new InsufficientBalanceException("Insufficient balance");
        }

        wallet.setBalance(walletBalance);

        wallet.setDateModified(auditDate);

        walletRepository.save(wallet);

        String narration = Optional.ofNullable(transactionRequest.getNarration()).orElse(transactionType.getName());
        final Transaction transaction = new Transaction(transactionType, wallet, amountToEffect,
                auditDate, narration,
                auditDate, transactionRequest.getSourceReference(), deviceId);
        systemReference(transaction);

        final Transaction savedTransaction = transactionRepository.save(transaction);
        processCommission(savedTransaction, wallet);
        return savedTransaction;
    }

    private void processCommission(final Transaction transaction, final Wallet wallet) {
        WalletGradeKey walletGradeKey = new WalletGradeKey();
        walletGradeKey.setTransactionType(transaction.getTransactionType());
        walletGradeKey.setWalletGrade(wallet.getGrade());
        final WalletGradeCommission commissionGrade = walletGradeCommissionRepository.findOne(walletGradeKey);
        if (commissionGrade == null) {
            log.info("Commission not set for: {}", walletGradeKey);
            return;
        }
        final Commission commission = new Commission();
        commission.setAmount(commissionGrade.calculate(transaction.getAmount()));
        commission.setCommissionStatus(CommissionStatus.PENDING);
        commission.setTransaction(transaction);
        commission.setRate(commissionGrade.getRate());
        commissionRepository.save(commission);
    }

    public Transaction reverse(final TransactionRequest request, String devideId) {
        log.info("Reversing transaction: device-id: {}, request: {}", devideId, request);
        final Optional<Transaction> optional = transactionRepository.findBySourceReferenceAndDeviceId(request.getSourceReference(), devideId);
        if (optional.isPresent()) {
            return processReversal(request.getNarration(), optional.get());
        }
        throw new BusinessException("Transaction not found");
    }

    @Transactional
    public Transaction reverse(Long transactionId, String narration) {

        final Transaction transactionToReverse = transactionRepository
                .findOne(transactionId);

        return processReversal(narration, transactionToReverse);
    }

    private Transaction processReversal(String narration, Transaction transactionToReverse) {
        if(transactionToReverse.isReversed()){
            throw new BusinessException("Transaction already reversed");
        }
        final BigDecimal amountToEffect = transactionToReverse.getAmount().negate();
        final Wallet wallet = transactionToReverse.getWallet();
        wallet.setBalance(wallet.getBalance().add(amountToEffect));
        final Date auditDate = new Date();
        wallet.setDateModified(auditDate);

        narration = Optional.ofNullable(narration).orElse(transactionToReverse.getNarration() + "- Reversal");

        final Transaction reversal = new Transaction(transactionToReverse.getTransactionType(),
                transactionToReverse.getWallet(), amountToEffect, BigDecimal.ZERO,
                auditDate, narration,
                auditDate, true, transactionToReverse);

        reversal.setReversed(true);
        transactionToReverse.setReversed(true);

        transactionRepository.save(transactionToReverse);

        return transactionRepository.save(reversal);
    }

    private void systemReference(final Transaction transaction) {
        final StringBuilder builder = new StringBuilder();
        builder.append(String.format("%02d", transaction.getTransactionType().getId()));
        builder.append(String.format("%04d", transaction.getWallet().getId()));
        builder.append(DateTimeFormatter.ofPattern("ddMMyyHHmmssSSS").format(LocalDateTime.now()));
        transaction.setSystemReference(builder.toString());
    }

    public Transaction findOne(Long transactionId) {
        return transactionRepository.findOne(transactionId);
    }


    public Page<Transaction> findWalletTransactions(final Long walletId, final FindTransactionRequest request, Pageable pageable){

        log.info("find wallet transactions {}, walletId:{}", request, walletId);

        Date startOfDay = getStartOfDay(request.getFromDate());

        Date endOfDay = getEndOfDay(request.getToDate());

        log.info("find wallet transactions {}, walletId:{}, from: {}, to: {}", request, walletId, startOfDay, endOfDay);
        return transactionRepository.findByWallet_IdAndTransactionDateBetween(walletId, startOfDay, endOfDay, pageable);
    }

    private Date getStartOfDay(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DATE);
        calendar.set(year, month, day, 0, 0, 0);
        return calendar.getTime();
    }

    private Date getEndOfDay(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DATE);
        calendar.set(year, month, day, 23, 59, 59);
        return calendar.getTime();
    }

}
