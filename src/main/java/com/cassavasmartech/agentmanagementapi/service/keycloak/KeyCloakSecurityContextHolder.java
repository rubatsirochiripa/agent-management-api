package com.cassavasmartech.agentmanagementapi.service.keycloak;

import com.cassavasmartech.agentmanagementapi.exception.BusinessException;
import org.keycloak.KeycloakPrincipal;
import org.keycloak.KeycloakSecurityContext;
import org.keycloak.representations.AccessToken;
import org.springframework.security.core.Authentication;

import java.util.Optional;

public class KeyCloakSecurityContextHolder {
    public static Optional<AccessToken> getAccessToken(){
        Optional<KeycloakSecurityContext> keycloakPrincipal = getKeycloakPrincipal();
        if(keycloakPrincipal.isPresent()){
            return Optional.ofNullable(keycloakPrincipal.get().getToken());
        }
        return Optional.empty();
    }

    public static AccessToken getRequiredUserToken(){
       return  KeyCloakSecurityContextHolder.getAccessToken()
               .orElseThrow(()-> new BusinessException("User not found"));
    }

    private static Optional<KeycloakSecurityContext> getKeycloakPrincipal() {
        Authentication authentication = org.springframework.security.core.context.SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null) {
            Object principal = authentication.getPrincipal();
            if (principal instanceof KeycloakPrincipal) {
                return Optional.ofNullable(KeycloakPrincipal.class.cast(principal).getKeycloakSecurityContext());
            }
        }
        return Optional.empty();
    }
}
