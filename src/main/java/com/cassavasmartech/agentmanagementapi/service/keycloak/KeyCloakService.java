package com.cassavasmartech.agentmanagementapi.service.keycloak;

import com.cassavasmartech.agentmanagementapi.api.data.UserCredentials;
import com.cassavasmartech.agentmanagementapi.api.data.UserDTO;
import com.cassavasmartech.agentmanagementapi.exception.BusinessException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.keycloak.admin.client.KeycloakBuilder;
import org.keycloak.admin.client.resource.RealmResource;
import org.keycloak.admin.client.resource.UsersResource;
import org.keycloak.representations.idm.CredentialRepresentation;
import org.keycloak.representations.idm.RoleRepresentation;
import org.keycloak.representations.idm.UserRepresentation;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import javax.ws.rs.core.Response;
import java.util.Arrays;

@Service
@Slf4j
@RequiredArgsConstructor
public class KeyCloakService {

    private final RestTemplate restTemplate;
    @Value("${keycloak.credentials.secret}")
    private String SECRETKEY;
    @Value("${keycloak.resource}")
    private String CLIENTID;
    @Value("${keycloak.auth-server-url}")
    private String AUTHURL;
    @Value("${keycloak.realm}")
    private String REALM;
    @Value("${key_cloak.default-user-role}")
    private String defaultRole;
    @Value("${key_cloak.admin-username}")
    private String keyCloakAdminUserName;
    @Value("${key_cloak.admin-password}")
    private String keyCloakAdminPassword;

    public ResponseEntity getToken(UserCredentials userCredentials) throws Exception {
        final String username = userCredentials.getUsername();
        final MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
        map.add("grant_type", "password");
        map.add("client_id", CLIENTID);
        map.add("username", username);
        map.add("password", userCredentials.getPassword());
        map.add("client_secret", SECRETKEY);
        return sendPost(map);
    }

    public ResponseEntity getByRefreshToken(String refreshToken) {
        final MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
        map.add("grant_type", "refresh_token");
        map.add("client_id", CLIENTID);
        map.add("refresh_token", refreshToken);
        map.add("client_secret", SECRETKEY);
        return sendPost(map);
    }

    public int createUserInKeyCloak(UserDTO userDTO) {
        int statusId = 0;
        try {
            final UsersResource userRessource = getKeycloakUserResource();
            final UserRepresentation user = getUserRepresentation(userDTO);
            final Response result = userRessource.create(user);
            statusId = result.getStatus();
            if (statusId == HttpStatus.CREATED.value()) {
                String userId = result.getLocation().getPath().replaceAll(".*/([^/]+)$", "$1");
                resetPassword(userDTO.getPassword(), userId);
                setDefaultDefaultRole(userId);
                log.info("User {} created successfully", userDTO.getUserName());
            } else if (statusId == HttpStatus.CONFLICT.value()) {
                log.warn("User {} already exists", userDTO.getUserName());
            } else {
                log.warn("User {} not created", userDTO.getUserName());
            }

        } catch (Exception e) {
            log.error("", e);
            throw new BusinessException(e.getLocalizedMessage());
        }
        return statusId;
    }

    private void setDefaultDefaultRole(String userId) {
        final RealmResource realmResource = getRealmResource();
        RoleRepresentation savedRoleRepresentation = realmResource.roles().get(defaultRole).toRepresentation();
        realmResource.users().get(userId).roles().realmLevel().add(Arrays.asList(savedRoleRepresentation));
    }

    private UserRepresentation getUserRepresentation(UserDTO userDTO) {
        UserRepresentation user = new UserRepresentation();
        user.setUsername(userDTO.getUserName());
        user.setEmail(userDTO.getEmailAddress());
        user.setFirstName(userDTO.getFirstName());
        user.setLastName(userDTO.getLastName());
        user.setEnabled(true);
        return user;
    }

    public void logoutUser(String userId) {

        UsersResource userRessource = getKeycloakUserResource();
        userRessource.get(userId).logout();
    }

    public void resetPassword(String newPassword, String userId) {
        final UsersResource userResource = getKeycloakUserResource();
        CredentialRepresentation passwordCred = new CredentialRepresentation();
        passwordCred.setTemporary(false);
        passwordCred.setType(CredentialRepresentation.PASSWORD);
        passwordCred.setValue(newPassword.trim());
        userResource.get(userId).resetPassword(passwordCred);
    }

    private UsersResource getKeycloakUserResource() {
        return getRealmResource().users();
    }

    private RealmResource getRealmResource() {
        return KeycloakBuilder.builder()
                .serverUrl(AUTHURL)
                .realm("master")
                .username(keyCloakAdminUserName)
                .password(keyCloakAdminPassword)
                .clientId("admin-cli")
                .resteasyClient(new ResteasyClientBuilder().connectionPoolSize(10).build())
                .build().realm(REALM);
    }

    private ResponseEntity sendPost(MultiValueMap<String, String> map) {
            String authUrl = AUTHURL + "/realms/" + REALM + "/protocol/openid-connect/token";
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
            HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(map, headers);
           return restTemplate.postForEntity(authUrl, request, String.class);
    }

}
