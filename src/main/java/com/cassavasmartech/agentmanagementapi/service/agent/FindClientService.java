package com.cassavasmartech.agentmanagementapi.service.agent;

import com.cassavasmartech.agentmanagementapi.model.Agent;

public interface FindClientService {
   Agent findOne(Long clientId);
}
