package com.cassavasmartech.agentmanagementapi.service.agent;

import com.cassavasmartech.agentmanagementapi.model.AccountStatus;
import com.cassavasmartech.agentmanagementapi.model.Agent;
import com.cassavasmartech.agentmanagementapi.repository.AgentRepository;
import com.cassavasmartech.agentmanagementapi.service.approval.ApprovalHandler;
import com.cassavasmartech.agentmanagementapi.service.approval.ApprovalType;
import com.cassavasmartech.agentmanagementapi.service.wallet.WalletService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;

import static com.cassavasmartech.agentmanagementapi.service.approval.ApprovalConstants.VariableNames.RECORD_IDENTIFIER;
import static com.cassavasmartech.agentmanagementapi.service.approval.ApprovalConstants.VariableNames.TARGET_STATUS;
import static com.cassavasmartech.agentmanagementapi.service.approval.ApprovalType.AGENT_CREATION;

@Service
@RequiredArgsConstructor
@Slf4j
public class AgentCreationApprovalHandler implements ApprovalHandler {

    private final AgentRepository agentRepository;

    private final WalletService walletService;

    @Override
    public boolean canApprove(ApprovalType approvalType) {
        return AGENT_CREATION == approvalType;
    }

    @Override
    @Transactional
    public void handleApproval(boolean accepted, ApprovalType approvalType, Map<String, Object> variables) {
        Long recordIdentifier = Long.parseLong(String.valueOf(variables.get(RECORD_IDENTIFIER)));
        log.info("Processing approval for agent id:{}", recordIdentifier);
        String targetStatus = String.valueOf(variables.get(TARGET_STATUS));
        log.info("targetStatus for agent id:{}", targetStatus);
        AccountStatus status;
        if (targetStatus == null) {
            status = AccountStatus.ACTIVE;
        } else {
            try {
                status = AccountStatus.valueOf(targetStatus);
            } catch (Exception ex) {
                status = AccountStatus.ACTIVE;
            }
        }
        final Agent agent = agentRepository.findOne(recordIdentifier);
        if (agent == null) {
            log.warn("Agent not found: {}", recordIdentifier);
            return;
        }
        if (accepted) {
            agent.setStatus(status);
            agentRepository.save(agent);
            walletService.createDefaultWallet(agent);
        }
        else{
            agent.setStatus(AccountStatus.REJECTED);
            agentRepository.save(agent);
        }
    }
}
