package com.cassavasmartech.agentmanagementapi.service.agent;

import com.cassavasmartech.agentmanagementapi.api.data.UploadedDocument;
import com.cassavasmartech.agentmanagementapi.documentmanagement.DocumentManagementServiceImpl;
import com.cassavasmartech.agentmanagementapi.model.Agent;
import com.cassavasmartech.agentmanagementapi.repository.AgentRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class AgentDocumentation {

    @Value("${mayan-document-upload-delay-mills}")
    private long uploadDelayInMills;

    final DocumentManagementServiceImpl documentManagementService;

    final AgentRepository agentRepository;



    public void upload(final Long agentId, final Long documentTypeId, MultipartFile file) throws Exception {
        final Agent agent = agentRepository.findOne(agentId);
        Long fileCabinetId = agent.getFileCabinetId();
        int documentId = documentManagementService.uploadDocument(documentTypeId, fileCabinetId, file);
        documentManagementService.addFileToCabinet(fileCabinetId, documentId);
        Thread.sleep(uploadDelayInMills);
    }

    public List<UploadedDocument> findAgentDocuments(long agentId){
        final Agent agent = agentRepository.findOne(agentId);
        log.info("Documents for: {}, cabineId: {}", agent, agent.getFileCabinetId());
       return documentManagementService.findCabinetDocuments(agent.getFileCabinetId());
    }
}
