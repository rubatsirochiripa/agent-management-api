package com.cassavasmartech.agentmanagementapi.service.agent;


import com.cassavasmartech.agentmanagementapi.model.Agent;
import com.cassavasmartech.agentmanagementapi.repository.AgentRepository;
import org.springframework.transaction.annotation.Transactional;


@Transactional(readOnly = true)
public class FindClientServiceImpl implements FindClientService {

    private final AgentRepository agentRepository;

    public FindClientServiceImpl(AgentRepository agentRepository) {
        this.agentRepository = agentRepository;
    }

    @Override
    public Agent findOne(Long clientId) {
            return agentRepository.findOne(clientId);
    }
}
