package com.cassavasmartech.agentmanagementapi.service.agent;

import com.cassavasmartech.agentmanagementapi.api.data.AgentApproval;
import com.cassavasmartech.agentmanagementapi.api.data.CreateAgentRequest;
import com.cassavasmartech.agentmanagementapi.documentmanagement.DocumentManagementServiceImpl;
import com.cassavasmartech.agentmanagementapi.exception.BusinessException;
import com.cassavasmartech.agentmanagementapi.model.AccountStatus;
import com.cassavasmartech.agentmanagementapi.model.Agent;
import com.cassavasmartech.agentmanagementapi.model.Currency;
import com.cassavasmartech.agentmanagementapi.repository.AgentRepository;
import com.cassavasmartech.agentmanagementapi.repository.CurrencyRepository;
import com.cassavasmartech.agentmanagementapi.service.approval.ApprovalConstants;
import com.cassavasmartech.agentmanagementapi.service.approval.ApprovalService;
import com.cassavasmartech.agentmanagementapi.service.keycloak.KeyCloakSecurityContextHolder;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.keycloak.representations.AccessToken;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static com.cassavasmartech.agentmanagementapi.service.approval.ApprovalConstants.VariableNames.APPROVAL_TYPE;
import static com.cassavasmartech.agentmanagementapi.service.approval.ApprovalConstants.VariableNames.ASSIGNEE;
import static com.cassavasmartech.agentmanagementapi.service.approval.ApprovalConstants.VariableNames.DESCRIPTION;
import static com.cassavasmartech.agentmanagementapi.service.approval.ApprovalConstants.VariableNames.INITIATOR;
import static com.cassavasmartech.agentmanagementapi.service.approval.ApprovalConstants.VariableNames.RECORD_IDENTIFIER;
import static com.cassavasmartech.agentmanagementapi.service.approval.ApprovalConstants.VariableNames.TARGET_STATUS;
import static com.cassavasmartech.agentmanagementapi.service.approval.ApprovalType.AGENT_CREATION;

@Service
@RequiredArgsConstructor
@Slf4j
public class AgentServiceImpl {

    private final AgentRepository agentRepository;

    private final ApprovalService approvalService;

    private final DocumentManagementServiceImpl documentManagementService;

    private final CurrencyRepository currencyRepository;

    public Agent findById(Long clientId) {
        if (clientId <= 0) {
            throw new BusinessException("Client id cannot be negative or zero");
        }
        return agentRepository.findOne(clientId);
    }

    public Page<Agent> search(final String searchTerm, Pageable pageable) {
        return agentRepository.findByNameContainingIgnoreCase(searchTerm, pageable);
    }

    public Optional<Agent> findByMobileNumber(final String mobileNumber, Pageable pageable) {
        return agentRepository.findByContactInfo_MobileNumber(mobileNumber);
    }

    public Optional<Agent> findByIdNumber(final String idNumber) {
        return agentRepository.findByIdNumber(idNumber);
    }

    public Page<Agent> findByIdNumberOrMobile(final String idNumber, String mobileNumber, Pageable pageable) {
        return agentRepository.findByIdNumberContainingOrContactInfo_MobileNumberContaining(idNumber, mobileNumber, pageable);
    }

    public Page<AgentApproval> findApprovals(String mobileNumber, Pageable pageable) {

        log.info("finding agent approvals by mobileNumber: {}", mobileNumber);

        if (StringUtils.isEmpty(mobileNumber)) {
            return agentRepository.findByStatus(AccountStatus.NEW, pageable)
                    .map(this::mapClient);
        }

        Optional<Agent> optionalAgent = agentRepository.findByContactInfo_MobileNumber(mobileNumber);

        if (optionalAgent.isPresent()) {
            return new PageImpl<>(Collections.singletonList(mapClient(optionalAgent.get())));
        }
        return new PageImpl<>(Collections.EMPTY_LIST);

    }

    private AgentApproval mapClient(final Agent agent) {
        final AgentApproval approval = new AgentApproval();
        approval.setAgent(agent);
        approval.setTasks(approvalService.getAllTasks(agent.getApprovalProcessInstanceId()));
        return approval;
    }

    @Transactional
    public Agent create(CreateAgentRequest createAgentRequest) {

        validateRequest(createAgentRequest);

        final Date auditDate = new Date();

        Optional<Currency> optional = currencyRepository.findByNumericCode(createAgentRequest.getCurrencyCode());
        if (!optional.isPresent()) {
            throw new BusinessException("invalid currency numeric code");
        }

        Currency currency = optional.get();
        final Agent agent = ClientBuilder
                .newInstance(createAgentRequest.getName(), createAgentRequest.getIdNumber(),
                        createAgentRequest.getCompany(), auditDate, currency)
                .contactInfo(createAgentRequest.getContactInfo()).surname(createAgentRequest.getSurname()).build();


        documentManagementService.createCabinet(agent.getContactInfo().getMobileNumber()).ifPresent(c -> agent.setFileCabinetId(c.getId()));

        Agent savedAgent = agentRepository.save(agent);

        String approvalProcessInstanceId = initiateApprovalProcess(savedAgent);

        savedAgent.setApprovalProcessInstanceId(approvalProcessInstanceId);

        agentRepository.save(savedAgent);

        return savedAgent;
    }

    @Transactional
    public Agent update(CreateAgentRequest createAgentRequest) {
        log.info("updating agent details");
        return null;
    }

    private String initiateApprovalProcess(Agent savedAgent) {
        AccessToken token = KeyCloakSecurityContextHolder.getRequiredUserToken();
        final Map<String, Object> variables = new HashMap<>();
        variables.put(ApprovalConstants.VariableNames.NAME, savedAgent.getContactInfo().getMobileNumber());
        variables.put(INITIATOR, token.getPreferredUsername());
        variables.put(DESCRIPTION, generateDescription(savedAgent));
        variables.put(RECORD_IDENTIFIER, savedAgent.getId());
        variables.put(ASSIGNEE, "NOT ASSIGNED");
        variables.put(APPROVAL_TYPE, AGENT_CREATION.name());
        variables.put(TARGET_STATUS, AccountStatus.ACTIVE.name());
        return approvalService.initiateApproval(variables);
    }

    private String generateDescription(Agent savedAgent) {
        return new StringBuilder("Account Creation")
                .append("\n")
                .append(savedAgent.getName())
                .append(" ")
                .append(savedAgent.getSurname())
                .append(" ")
                .append(savedAgent.getIdNumber()).toString();
    }

    private void validateRequest(CreateAgentRequest createAgentRequest) {

        Optional<Agent> agentOptional = agentRepository.findByContactInfo_MobileNumber(createAgentRequest.getContactInfo().getMobileNumber());
        if (agentOptional.isPresent()) {
            throw new BusinessException("agent with mobile number already exists");
        }


        if (createAgentRequest == null) {
            throw new IllegalArgumentException("Create client request is required");
        }
        if (StringUtils.isEmpty(createAgentRequest.getName())) {
            throw new BusinessException("invalid client name");
        }
        if (createAgentRequest.getCompany() == null) {
            throw new BusinessException("invalid company indicator");
        }
        final String idNumber = createAgentRequest.getIdNumber();
        if (!createAgentRequest.getCompany() && StringUtils.isEmpty(idNumber)) {
            throw new BusinessException("invalid id/registration number");
        }
        String mobileNumber = createAgentRequest.getContactInfo().getMobileNumber();
        agentRepository.findByContactInfo_MobileNumber(mobileNumber).ifPresent(client -> {
            throw new BusinessException("mobile number already exists");
        });

        if (createAgentRequest.getContactInfo() == null) {
            throw new BusinessException("invalid contact information");
        }

    }


}
