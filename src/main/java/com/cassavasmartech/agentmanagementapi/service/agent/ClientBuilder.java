package com.cassavasmartech.agentmanagementapi.service.agent;

import com.cassavasmartech.agentmanagementapi.model.Address;
import com.cassavasmartech.agentmanagementapi.model.Agent;
import com.cassavasmartech.agentmanagementapi.model.ContactInfo;
import com.cassavasmartech.agentmanagementapi.model.Currency;

import java.util.Date;

public class ClientBuilder {
    private Agent agent;

    private ClientBuilder(String name, String idNumber, boolean company, Date dateCreated, Currency currency) {
        this.agent = new Agent();
        this.agent.setName(name);
        this.agent.setIdNumber(idNumber);
        this.agent.setCompany(company);
        this.agent.setDateCreated(dateCreated);
        this.agent.setDateModified(dateCreated);
        this.agent.setDefaultCurrency(currency);
    }

    public ClientBuilder currency(Currency currency) {
        this.agent.setDefaultCurrency(currency);
        return this;
    }

    public ClientBuilder surname(String surname) {
        this.agent.setSurname(surname);
        return this;
    }

    public ClientBuilder contactInfo(ContactInfo contactInfo) {
        this.agent.setContactInfo(contactInfo);
        return this;
    }

    public ClientBuilder physicalAddress(Address address) {
        checkContactInfoSet();
        this.agent.getContactInfo().setPhysicalAddress(address);
        return this;
    }

    public ClientBuilder postalAddress(Address address) {
        checkContactInfoSet();
        this.agent.getContactInfo().setPostalAddress(address);
        return this;
    }

    private void checkContactInfoSet() {
        if (this.agent.getContactInfo() == null) {
            contactInfo(new ContactInfo());
        }
    }

    public ClientBuilder email(String email) {
        checkContactInfoSet();
        this.agent.getContactInfo().setEmail(email);
        return this;
    }

    public ClientBuilder telephone(String telephone) {
        checkContactInfoSet();
        this.agent.getContactInfo().setTelephone(telephone);
        return this;
    }

    public ClientBuilder mobileNumber(String mobileNumber) {
        checkContactInfoSet();
        this.agent.getContactInfo().setMobileNumber(mobileNumber);
        return this;
    }

    public static ClientBuilder newInstance(String name, String registrationNumber, boolean company,
                                            Date dateCreated,Currency currency ) {
        return new ClientBuilder(name, registrationNumber, company, dateCreated, currency);
    }

    public Agent build() {
        return this.agent;
    }
}
