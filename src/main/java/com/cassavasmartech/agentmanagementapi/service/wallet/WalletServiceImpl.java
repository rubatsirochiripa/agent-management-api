package com.cassavasmartech.agentmanagementapi.service.wallet;

import com.cassavasmartech.agentmanagementapi.api.data.WalletRequest;
import com.cassavasmartech.agentmanagementapi.exception.BusinessException;
import com.cassavasmartech.agentmanagementapi.model.AccountStatus;
import com.cassavasmartech.agentmanagementapi.model.Agent;
import com.cassavasmartech.agentmanagementapi.model.Currency;
import com.cassavasmartech.agentmanagementapi.model.Wallet;
import com.cassavasmartech.agentmanagementapi.model.WalletGrade;
import com.cassavasmartech.agentmanagementapi.repository.AgentRepository;
import com.cassavasmartech.agentmanagementapi.repository.CurrencyRepository;
import com.cassavasmartech.agentmanagementapi.repository.WalletGradeRepository;
import com.cassavasmartech.agentmanagementapi.repository.WalletRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Slf4j
public class WalletServiceImpl implements WalletService {
    private final WalletRepository walletRepository;
    private final CurrencyRepository currencyRepository;
    private final AgentRepository agentRepository;
    private final WalletGradeRepository walletGradeRepository;

    @Override
    @Transactional
    public Wallet create(WalletRequest walletRequest, final Long agentId) {
        log.info("Creating account for request {}", walletRequest);
        Assert.notNull(walletRequest, "Create account request is required");
        final Agent agent = agentRepository.findOne(agentId);
        if (agent == null) {
            throw new BusinessException("invalid agent id provided");
        }
        Optional<Currency> optionalCurrency = currencyRepository.findByAlphaCode(walletRequest.getCurrencyCode());
        if (!optionalCurrency.isPresent()) {
            throw new BusinessException("invalid currency code");
        }
        Currency currency = optionalCurrency.get();


        final WalletGrade grade = walletGradeRepository.findOne(walletRequest.getWalletGradeId());
        if (grade == null) {
            throw new BusinessException("invalid wallet grade");
        }

        final Wallet savedWallet = createWallet(agent, currency, grade, AccountStatus.NEW);
        log.info("Created account {} for request {}", savedWallet, walletRequest);
        return savedWallet;
    }

    private Wallet createWallet(Agent agent, Currency currency, WalletGrade grade, AccountStatus accountStatus) {
        final Date auditDate = new Date();
        final String mobileNumber = agent.getContactInfo().getMobileNumber();
        final String accountNumber = mobileNumber + currency.getNumericCode();
        final Wallet wallet = new Wallet(accountNumber, agent, auditDate, currency, grade, accountStatus);

        final Wallet savedWallet = walletRepository.save(wallet);
        return savedWallet;
    }

    public Wallet createDefaultWallet(final Agent agent) {
        final Optional<WalletGrade> optional = walletGradeRepository.findByDefaultGradeTrue();
        if (optional.isPresent()) {
            return createWallet(agent, agent.getDefaultCurrency(), optional.get(), AccountStatus.ACTIVE);
        }
        throw new BusinessException("invalid wallet grade");
    }

    @Override
    public Wallet findOne(Long walletId) {
        return walletRepository.findOne(walletId);
    }

    public Optional<Wallet> findByAccountNumberAndStatus(String accountNumber, AccountStatus accountStatus) {
        return walletRepository.findByAccountNumberAndAccountStatus(accountNumber, accountStatus);
    }

    public List<Wallet> findByMobileNumber(final String mobileNumber){
        return walletRepository.findByAgent_ContactInfo_MobileNumber(mobileNumber);
    }

    @Override
    public Wallet update(WalletRequest walletRequest, Long walletId) {
        //find wallet with walletId
        //update the wallet account status
        return null;
    }


}
