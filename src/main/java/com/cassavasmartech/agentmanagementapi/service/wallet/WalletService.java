package com.cassavasmartech.agentmanagementapi.service.wallet;

import com.cassavasmartech.agentmanagementapi.api.data.WalletRequest;
import com.cassavasmartech.agentmanagementapi.model.Agent;
import com.cassavasmartech.agentmanagementapi.model.Wallet;
import com.cassavasmartech.agentmanagementapi.model.AccountStatus;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

public interface WalletService {
    @Transactional
    Wallet create(WalletRequest walletRequest, final Long agentId);

    Wallet update(WalletRequest walletRequest, final Long walletId);

    Wallet findOne(Long walletId);

    Wallet createDefaultWallet(final Agent agent);

    Optional<Wallet> findByAccountNumberAndStatus(String accountNumber, AccountStatus accountStatus);

    List<Wallet> findByMobileNumber(final String mobileNumber);
}
