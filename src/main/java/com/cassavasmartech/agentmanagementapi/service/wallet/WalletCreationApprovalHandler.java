package com.cassavasmartech.agentmanagementapi.service.wallet;

import com.cassavasmartech.agentmanagementapi.service.approval.ApprovalHandler;
import com.cassavasmartech.agentmanagementapi.service.approval.ApprovalType;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Map;

import static com.cassavasmartech.agentmanagementapi.service.approval.ApprovalType.WALLET_CREATION;

@Slf4j
@Service
@RequiredArgsConstructor
public class WalletCreationApprovalHandler implements ApprovalHandler {

    @Override
    public boolean canApprove(ApprovalType approvalType) {
        return WALLET_CREATION == approvalType;
    }

    @Override
    public void handleApproval(boolean accepted, ApprovalType approvalType, Map<String, Object> variables) {
    }
}
