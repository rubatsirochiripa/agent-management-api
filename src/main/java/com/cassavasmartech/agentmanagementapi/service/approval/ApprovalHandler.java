package com.cassavasmartech.agentmanagementapi.service.approval;

import org.springframework.transaction.annotation.Transactional;

import java.util.Map;

public interface ApprovalHandler {
   boolean canApprove(ApprovalType approvalType);

   @Transactional
   void handleApproval(boolean accepted, ApprovalType approvalType ,  Map<String,Object> variables);
}
