package com.cassavasmartech.agentmanagementapi.service.approval;

import com.cassavasmartech.agentmanagementapi.api.ApprovalRequest;
import com.cassavasmartech.agentmanagementapi.api.data.TaskRepresentation;
import com.cassavasmartech.agentmanagementapi.service.keycloak.KeyCloakSecurityContextHolder;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Comment;
import org.activiti.engine.task.Task;
import org.keycloak.representations.AccessToken;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.cassavasmartech.agentmanagementapi.service.approval.ApprovalConstants.VariableNames.TASK_APPROVED;
import static com.cassavasmartech.agentmanagementapi.service.approval.ApprovalConstants.VariableNames.TASK_COMPLETED_BY;

@Service
@Slf4j
@RequiredArgsConstructor
public class ApprovalServiceImpl implements ApprovalService {

    private final RuntimeService runtimeService;

    private final TaskService taskService;

    @Value("${activiti-approval-process-defination-key}")
    private String approvalProcessId;

    @Override
    public String initiateApproval(Map<String, Object> variables) {
        log.info("Initiating approval for {}", variables);
        final ProcessInstance pi = runtimeService.startProcessInstanceByKey(approvalProcessId, variables);
        taskService.createTaskQuery()
                .processInstanceId(pi.getId())
                .active()
                .list()
                .stream()
                .forEach(t -> {
                    log.info("adding task variables: {}", t.getId());
                    taskService.setVariables(t.getId(), variables);
                });
        log.info("Approval process started successfully.");
        return pi.getId();
    }

    public List<TaskRepresentation> getAllTasks(final String processInstanceId) {
        return taskService
                .createTaskQuery()
                .processInstanceId(processInstanceId)
                .list()
                .stream()
                .map(this::getTaskRepresentation)
                .collect(Collectors.toList());
    }

    private TaskRepresentation getTaskRepresentation(Task task) {
        return new TaskRepresentation(task.getId(), task.getName(), task.getAssignee(), task.getDescription(), task.getProcessInstanceId());
    }

    public void completeTask(String taskId,String processInstanceId, ApprovalRequest approvalRequest) {

        AccessToken token = KeyCloakSecurityContextHolder.getRequiredUserToken();

        log.info("Completing task {},{}", taskId, approvalRequest);

        final Map<String, Object> variables = new HashMap<>();

        variables.put(TASK_APPROVED, approvalRequest.getStatus().isApproved());

        variables.put(TASK_COMPLETED_BY, token.getPreferredUsername());

        if(!StringUtils.isEmpty(approvalRequest.getComment())){
            taskService.addComment(taskId, processInstanceId, approvalRequest.getComment());
        }

        taskService.complete(taskId, variables);

        log.info("Task Completed Successfully {},{}", taskId);
    }

}
