package com.cassavasmartech.agentmanagementapi.service.approval;

public interface ApprovalConstants {
    interface VariableNames{
        String INITIATOR= "INITIATOR" ;
        String DESCRIPTION = "description";
        String RECORD_IDENTIFIER = "recordIdentifier";
        String ASSIGNEE = "assignee";
        String APPROVAL_TYPE = "approvalType";
        String TASK_APPROVED = "taskApproved";
        String TASK_COMPLETED_BY = "taskCompletedBy";
        String TARGET_STATUS = "targetStatus";
        String NAME = "name";
    }
}
