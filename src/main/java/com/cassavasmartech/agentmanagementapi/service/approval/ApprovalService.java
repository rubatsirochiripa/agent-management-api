package com.cassavasmartech.agentmanagementapi.service.approval;

import com.cassavasmartech.agentmanagementapi.api.ApprovalRequest;
import com.cassavasmartech.agentmanagementapi.api.data.TaskRepresentation;
import org.activiti.engine.delegate.JavaDelegate;

import java.util.List;
import java.util.Map;

public interface ApprovalService  {
    String  initiateApproval(Map<String, Object> variables);
    List<TaskRepresentation> getAllTasks(final String processInstanceId);
    void completeTask(String taskId,String processInstanceId, ApprovalRequest approvalRequest);
}
