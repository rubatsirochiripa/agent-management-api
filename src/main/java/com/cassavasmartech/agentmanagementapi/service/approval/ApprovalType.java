package com.cassavasmartech.agentmanagementapi.service.approval;

public enum ApprovalType {
    AGENT_CREATION, TRANSACTION, WALLET_CREATION;
}
