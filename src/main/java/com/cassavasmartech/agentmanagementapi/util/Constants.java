package com.cassavasmartech.agentmanagementapi.util;

public interface Constants {
    interface AppHeaders{
        String X_HEADER_DEVICE_ID = "X-Header-DeviceId";
        String X_APP_ID = "X-App-Id";
    }
}
