package com.cassavasmartech.agentmanagementapi.util;

import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.math.RoundingMode;

public final class Utils {
    public static final BigDecimal ZERO_2_DECIMAL_PLACES = BigDecimal.valueOf(0.001);
    private static final String EMAIL_REGEX = "^[\\w-\\+]+(\\.[\\w]+)*@[\\w-]+(\\.[\\w]+)*(\\.[a-z]{2,})$";

    private Utils() {

    }

    public static BigDecimal doubleToBigDecimal(double amount) {
        BigDecimal amountToSet = BigDecimal.valueOf(amount);
        return amountToSet.setScale(5, RoundingMode.HALF_UP);
    }

    public static BigDecimal max(BigDecimal value1, BigDecimal value2) {
        int compareResult = value1.compareTo(value2);
        if (compareResult > 0) {
            return value1;
        }
        return value2;
    }

    public static BigDecimal min(BigDecimal value1, BigDecimal value2) {
        int compareResult = value1.compareTo(value2);
        if (compareResult < 0) {
            return value1;
        }
        return value2;
    }

    public static boolean isValidEmailAddress(String emailAddress) {
        return StringUtils.hasText(emailAddress) && emailAddress.matches(EMAIL_REGEX);
    }
}
